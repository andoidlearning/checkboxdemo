package com.example.checkboxradiobutton

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnOrder : Button = findViewById(R.id.btnOrder)
        val rbBeef : RadioButton = findViewById(R.id.rbBeef)
        val rbChicken : RadioButton = findViewById(R.id.rbChicken)
        val rbPark : RadioButton = findViewById(R.id.rbPark)
        val cbCheese : CheckBox = findViewById(R.id.cbCheese)
        val cbOnions : CheckBox = findViewById(R.id.cbOnions)
        val cbSalad : CheckBox = findViewById(R.id.cbSalad)
        val rbMeat : RadioGroup = findViewById(R.id.rbMeat)
        val tvOrder : TextView = findViewById(R.id.tvOrder)

        btnOrder.setOnClickListener {
            val chkRadioButton = rbMeat.checkedRadioButtonId
            val meat = findViewById<RadioButton>(chkRadioButton)
            val cheese = cbCheese.isChecked
            val onions = cbOnions.isChecked
            val salad = cbSalad.isChecked
            val orderString = "You ordered a burger with:\n"+
                    "${meat.text}"+
                    (if (cheese) "\nCheese" else "") +
                    (if (onions) "\nOnions" else "") +
                    (if (salad) "\nSalad" else "")

            tvOrder.text = orderString
        }

    }
}